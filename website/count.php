﻿<?php
	header("Content-Type:text/html;charset=utf-8");

	$filePath = './count_'.date("Ym").'.txt';

	//不存在则创建文件
	if (!file_exists($filePath)) {
		$fh = fopen($filePath, 'wb');
		fclose($fh);
	}

	//打开文件
	$handle = fopen($filePath, 'r+');
	//判断文件是否打开成功
	if (!$handle) {
		echo '文件打开失败！';
		return;
	}
	// 读取第一行
	$str = fgets($handle,1024);
	//关闭文件
	fclose($handle);
	
	if(false !== $str){
		$time = explode(',', $str)[0];
		if(date("m-d") == $time){
			$count = explode(',', $str)[1];
			$countNum =  number_format($count);
			$countNum++;
			$handle = fopen($filePath, 'r+');
			fwrite($handle, date("m-d").','.$countNum);
			echo $countNum;
			//关闭文件
			fclose($handle);
		}else{
			//在第一行前增加一行
			$header = date("m-d").','.'1'."\r\n";
  			$data = file_get_contents($filePath);
  			
  			$list = explode("\r\n",$data);
  			$newData = "";
  			if(count($list) > 59){
  				for($i=0; $i<59; $i++){
  					$newData = $newData.$list[$i]."\r\n";
  				}
  				file_put_contents($filePath, $header.$newData);
  			}else{
	  			file_put_contents($filePath, $header.$data);
  			}
  			echo 1;
		}
	}else{
		//在第一行前增加一行
		$header = date("m-d").','.'1'."\r\n";
		$data = file_get_contents($filePath);
		file_put_contents($filePath, $header.$data);
		echo 1;
	}
?>