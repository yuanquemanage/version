<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	$filePath = './msg.txt';

	//不存在则创建文件
	if (!file_exists($filePath)) {
		$fh = fopen($filePath, 'wb');
		fclose($fh);
		file_put_contents($filePath, 1);
	}
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		// 获取IP
		$user_IP = $_SERVER["REMOTE_ADDR"];
		
		// 获取参数
		$msg = $_POST['msg'];
		trim($msg);
		
		if(strlen($msg) > 20 && strlen($msg) < 1000){
			$msg = date("Y-m-d H:i")."<br />".$msg."<br />IP: ".$user_IP."<br />";

			$data = file_get_contents($filePath);
			
			$out = "";
			$arr = explode('<br />', $data);
			for($i=0; $i<count($arr) && $i<100; $i++){
				$out = $out.'<br />'.$arr[$i];
			}
		
			file_put_contents($filePath, $msg.$out);
		}
	}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>留言</title>
<script type="text/javascript">

</script>
<style>
html, body, a{background:#328aa4; color:#fff; font-size:1.2vw; line-height:1.6vw;}
td{height:28px;}
td input{min-height:25px; line-height:25px;}
#receiver, #selectReceiver{
	padding: 1px 0 1px 0;
    border-radius: 3px;
    font-size: 1vw;
    height:16px;
    min-height:25px;
    line-height:25px;
}
#msg{
	border-radius: 3px;
}
#open:hover{
	text-decoration:none;
}
.more{color:blue; margin-left:5px;}
#open:hover{font-weight:bolder;}
.about{color:#BBB; text-decoration:none;}
.about:hover{text-decoration:underline;}
</style>
</head>

<body>
<div id="out" style="max-width:980px; margin:0 auto;">
<!--main start-->
<div id="main" style="padding:1.5vw; padding-top:0px;">
    <div id="container" class="container" style="margin-top:10px;">
    	<div style="width:100%; height:2.5vw; margin-top:2.5vw;">
    		留言：<span style="color:#62aac4;">（ 问题请发邮件yuanquehelp@vip.qq.com ）</span>
    		<a href="index.html" style="float:right;">返回</a>
    	</div>
    	<form id="sendMsg" style="height:3.8vw; width:100%;" action="msg.php" method="post" onsubmit="return onSubmit()">
    		<input id="msg" name="msg" placeholder="请输入留言..." class="border inputTag" autocomplete="off" style="height:3vw; width:80%; float:left; padding-left:2px; border:solid 1px #ccc; text-indent:0.5vw; background-color:#E4FFFC;" maxlength="200"></input>
	        <input type="submit" value="发 送" class="submitBut" style="height:3.2vw; width:10%; float:left; margin-left:2%; text-align:center; border:solid 1px #ccc; cursor:pointer;" />
	    </form>
		<div style="width:100%; height:1.5vw; margin-top:1px; color:#62aac4; font-size:1vw;">
    		> 10字符
    	</div>
    	<div style="width:100%; height:2.5vw; margin-top:1.5vw;">
    		历史：
    	</div>
    	<div style="border:1px solid #CCC; padding:1.5vw; border-radius:4px; color:#ddd; font-size:1vw; line-height:1.8vw;">
	    	<?php
				$data = file_get_contents($filePath);
				echo $data;
			?>
	    </div>
	</div>
	<div style="width:100%; font-size:1.1vw; height:2.5vw; line-height:2.5vw; text-align:center; position:fixed; bottom:0; left:0; background-color:#328aa4; color:#BBB;">
		<span id="count"></span>
		问题反馈：yuanquehelp@vip.qq.com
		，<a class="about" href="msg.php">留言</a>
		，<a class="about" href="about.html">关于</a>
	</div>
</div>
<!--main end-->
</div>
</body>
</html>